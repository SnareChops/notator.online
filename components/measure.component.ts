import {Component, NgZone} from '@angular/core';
import {MeasureService} from '../services/measure.service';
import {Selectable, SelectionContext} from '../interfaces/selectable.interface';
import {SelectionService} from '../services/selection.service';

@Component({
	selector: 'measure',
	template: `
	<svg [style.width]="measureLength">
		<line x1="0" [attr.x2]="measureLength" y1="10" y2="10" stroke="black" stroke-width="2"/>
		<line x1="0" [attr.x2]="measureLength" y1="20" y2="20" stroke="black" stroke-width="2"/>
		<line x1="0" [attr.x2]="measureLength" y1="30" y2="30" stroke="black" stroke-width="2"/>
		<line x1="0" [attr.x2]="measureLength" y1="40" y2="40" stroke="black" stroke-width="2"/>
		<line x1="0" [attr.x2]="measureLength" y1="50" y2="50" stroke="black" stroke-width="2"/>
	</svg>
	<whole-rest></whole-rest>
	<single-barline></single-barline>
	`,
	host:{
		'[class.selected]': 'selected',
		'(click)': 'select($event)'
	}
})
export class MeasureComponent implements Selectable{
	public selectionContext: SelectionContext = 'measure';
	public selected: boolean = false;
	public measureLength: number = 100;

	constructor(public ngZone: NgZone, private selectionService: SelectionService){
	}

	public select(event: Event): void{
		event.stopPropagation();
		this.selectionService.onSelection.next(this);
		this.selected = true;
	}
}