import {Component} from '@angular/core';

@Component({
	selector: 'single-barline',
	template: `
	<svg>
		<line x1="0" x2="0" y1="0" y2="40" stroke="black" stroke-width="1"/>
	</svg>`
})
export class SingleBarlineComponent{}