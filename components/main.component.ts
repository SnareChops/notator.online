import {Component, OnInit} from '@angular/core';
import * as mousetrap from 'mousetrap';
import {SelectionService} from '../services/selection.service';

@Component({
    selector: 'notator',
    template: `
    <router-outlet></router-outlet>
    <measure></measure>
    <measure></measure>
    <measure></measure>
    <measure></measure>
    <measure></measure>
    `,
	host: {
    	'(click)': 'select($event)'
	}
})
export class MainComponent implements OnInit{

	constructor(private selectionService: SelectionService){
	}

	public ngOnInit(){
		mousetrap.bind('mod+b', this.addMeasure.bind(this));
	}

	public addMeasure(){

	}

	public select(event: Event){
		event.stopPropagation();
		this.selectionService.onSelection.next(void 0);
	}
}