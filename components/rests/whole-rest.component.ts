import {Component} from '@angular/core';

@Component({
	selector: 'whole-rest',
	template: `
	<svg>
		<rect height="5" width="10" fill="black"></rect>
	</svg>
	`
})
export class WholeRestComponent{}