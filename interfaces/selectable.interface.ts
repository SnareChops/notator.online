import {NgZone} from '@angular/core';
export type SelectionContext = 'note' | 'barline'| 'clef' | 'rest' | 'measure';

export interface Selectable{
	selectionContext: SelectionContext;
	selected: boolean;
	ngZone: NgZone;
}