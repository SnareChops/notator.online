# notator.online

## FEATURES FOR INITIAL ALPHA RELEASE

- [ ] Add a measure using `mod+b`
- [ ] Place a note by typing the name of the note
- [ ] Place a note by repeating a note with `r`
- [x] Select a measure
- [ ] Select a note
- [ ] Set time signature by right clicking on time signature and choosing from the selection
- [ ] Set clef type by right clicking on clef and choosing from the selection
- [ ] Initial time signature of 4/4 added
- [ ] Initial clef of treble added
- [ ] Set key signature right clicking on key signature and choosing from the selection
- [ ] Initial key signature of C added
- [ ] Set value of note by selecting and pressing a number key
- [ ] Navigate between notes by `right` and `left` arrow keys
- [ ] Change pitch of note by selecting and using `up` and `down` arrow keys or pressing note name
- [ ] Set note to sharp or flat by selecting and pressing a key choosing the accidental
- [ ] Change note to a rest using [TBD] key