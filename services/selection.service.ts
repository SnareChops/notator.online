import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {Selectable} from '../interfaces/selectable.interface';

@Injectable()
export class SelectionService{
	public onSelection: Subject<Selectable> = new Subject<Selectable>();
	public selectedItem: Selectable;

	constructor(){
		this.onSelection.subscribe((item) => {
			if(this.selectedItem) {
				this.selectedItem.ngZone.run(() => {
					this.selectedItem.selected = false;
				});
			}
			this.selectedItem = item;
		});
	}
}