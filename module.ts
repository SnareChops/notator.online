import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {MainComponent} from './components/main.component';
import {TrebleClefComponent} from './components/clefs/treble-clef.component';
import {QuarterNoteComponent} from './components/notes/quarter-note.component';
import {MeasureComponent} from './components/measure.component';
import {MeasureService} from './services/measure.service';
import {SingleBarlineComponent} from './components/barlines/single-barline.component';
import {WholeRestComponent} from './components/rests/whole-rest.component';
import {RestService} from './services/rest.service';
import {SelectionService} from './services/selection.service';
import {MeasuresPlaceholderDirective} from './directives/measures-placeholder.directive';

@NgModule({
	imports: [BrowserModule],
	declarations: [
		MainComponent,
		MeasureComponent,

		// Barlines
		SingleBarlineComponent,

		// Clefs
		TrebleClefComponent,

		// Notes
		QuarterNoteComponent,

		// Rests
		WholeRestComponent,

		// Directives
		MeasuresPlaceholderDirective
	],
	providers:[
		MeasureService,
		SelectionService,
		RestService
	],
	bootstrap: [MainComponent]
})
export class NotatorModule{}