import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {NotatorModule} from './module';

platformBrowserDynamic().bootstrapModule(NotatorModule);