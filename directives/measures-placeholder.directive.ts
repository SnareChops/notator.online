import {Directive} from '@angular/core';

@Directive({
	selector: 'measures-placeholder'
})
export class MeasuresPlaceholderDirective{}